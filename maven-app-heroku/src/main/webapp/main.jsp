<%@page import="uts.wsd.*"%>
<!-- Source: https://github.com/DamienFremont/blog/tree/master/20151007-javaee-webjars-->
<!-- Complete Dashboard source in bootstrap 4: https://github.com/modularcode/modular-admin-html-->
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="webjars/bootstrap/3.3.5/css/bootstrap.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script type="text/javascript" src="script.js"></script>  
        <link rel="stylesheet" type="text/css" href="style.css">         
    </head>
    <body>
        <%
            User user = (User) session.getAttribute("user");
            if(user != null){
                String log = " &lt " + user.getName() + " &gt";
            
        %>

        <div class="bs-docs-header" id="content" tabindex="-1">
            <div class="container">
                <h1>DRA Framework for IOT</h1>
                <p>A DevOps Reference Architecture Pipeline for Multi-Cloud.</p>
            </div>
            <nav class="navbar navbar-inverse">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span> 
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="#Display-Div" data-toggle="tab" id="IOT-Network">IoT-Network</a>
                    </div>
                    <div class="collapse navbar-collapse">
                        <ul class="nav navbar-nav"> 
                            <li class="dropdown">
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">Author<span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a href="https://www.linkedin.com/in/georges-bou-ghantous/" target="_Blank" style="color:#cdbfe3;">Linked In</a></li>
                                    <li><a href="mailto:Georges.BouGhantous-1@uts.edu.au" style="color:#cdbfe3; ">Contact</a></li>
                                    <li><a href="https://www.uts.edu.au/" target="_Blank" style="color:#cdbfe3; ">Institution</a></li>
                                </ul>
                            </li>
                            <li class="dropdown">
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">Publications<span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a href="https://goo.gl/91XnCi" target="_Blank" style="color:#cdbfe3;">PACIS2017 - Conference</a></li>  
                                    <li><a href="https://goo.gl/Vy5aT9" target="_Blank" style="color:#cdbfe3;">IEEE-CBI2018 - Conference</a></li>
                                    <li><a href="https://drive.google.com/file/d/1QvCZVHLGCji2TRJFR_L-wT-MvsjKKyOt/view?usp=sharing" target="_Blank" style="color:#cdbfe3;">IJLTER2019-Journal</a></li>
                                </ul>
                            </li>                            
                            <li class="dropdown">
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">DRA 1.0<span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a href="https://goo.gl/nq8vJc" target="_Blank" style="color:#cdbfe3;">DRA v1.0 Project</a></li>                                    
                                    <li><a href="https://youtu.be/IJP9qYcatHs" target="_Blank" style="color:#cdbfe3; ">DRA v1.0: Demo Video</a></li>
                                    <li><a href="#Display-Div" data-toggle="tab" data-target="#Display-Div"style="color:#cdbfe3;" id="DRA1-View">DRAv1.0 View</a></li>                                     
                                </ul>
                            </li> 
                            <li class="dropdown">
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">DRA v2.0<span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a href="https://goo.gl/3PYfPh" target="_Blank" style="color:#cdbfe3;">DRA v2.0 Project</a></li>                                    
                                    <li><a href="https://youtu.be/DmrIAciPKAU" target="_Blank" style="color:#cdbfe3; ">DRA v2.0: Demo Video</a></li>
                                    <li><a href="#Display-Div" data-toggle="tab" data-target="#Display-Div"style="color:#cdbfe3;" id="DRA2-View">DRA2.0 View</a></li>                                     
                                </ul>
                            </li>
                            <li class="dropdown">
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">DRA+APEM<span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a href="https://goo.gl/fUE8hY" target="_Blank" style="color:#cdbfe3;">DRA+APEM Integration</a></li>                                 
                                    <li><a href="#Display-Div" data-toggle="tab" data-target="#Display-Div"style="color:#cdbfe3;" id="DRA3-View">DRA+APEM View</a></li>
                                </ul>
                            </li>
                            <li class="dropdown">
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">DRA Short Course<span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a href="https://docs.google.com/presentation/d/1Zg3xJ7rOu3LqL7QMpJZPXD4iNdfpaznTaljKAYcOSkQ/edit?usp=sharing" target="_Blank" style="color:#cdbfe3;">DRA Short Course Slides</a></li>                                 
                                    <li><a href="https://asd-demo-app.herokuapp.com/" target="_Blank" style="color:#cdbfe3;">DRA Short Demo App</a></li> 
                                    <li><a href="https://docs.google.com/forms/d/1E4jU-XU1eXt6-xb3xAVrOcQGjPUDDaXU7omSp-GkIa4/edit?usp=sharing" target="_Blank" style="color:#cdbfe3;">Course Assessment</a></li>
                                    <li><a href="https://docs.google.com/forms/d/1NIO1U1oYiHcFHwp1HvaTXzeok3fH-6M5T4_kvBIcgUg/edit?usp=sharing" target="_Blank" style="color:#cdbfe3;">Course Evaluation</a></li>
                                </ul>
                            </li>
                            <li class="dropdown">
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">DRA Evaluation<span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a href="https://goo.gl/bM4w1t" target="_Blank" style="color:#cdbfe3;">Google Survey Form</a></li>  
                                    <li><a href="https://docs.google.com/document/d/1tx177JIdXKWm0CipS7ZtoYw65g88qtqpASAZC8vRStU/edit?usp=sharing" target="_Blank" style="color:#cdbfe3;">Case Study Template</a></li>  
                                </ul>
                            </li>
                        </ul>                                                
                        <ul class="nav navbar-nav navbar-right">                            
                            <li><a href="logout.jsp" id="logout" ><span style="color:#00ffcc">You are logging in as: <%=log%></span>&emsp;<span class="glyphicon glyphicon-log-in"></span> Logout</a></li>
                            <%}else{%>
                            <li><a href="logout.jsp" id="logout" ><span style="color:#00ffcc">No Existing User!</span>&emsp;<span class="glyphicon glyphicon-log-in"></span> Logout</a></li>
                            <%}%>
                        </ul>                         
                    </div>
                </div>
            </nav>            
        </div>
        <div class="container" style="width:100%; height: 70%; " id="IoT-div">
            <div class="row">                                  
                <div class="tab-content" id="Display-Div">
                    <iframe id="IOT-frame" src="" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe> 
                </div>                
            </div>
        </div>

        <!-- Quick Access: Icons -->
        <div class="footer"> 
            <div class="bs-glyphicons" style="padding-bottom:10px;">                                
                <h2 id="buttons-options">DRA Pipeline Quick Access</h2>
                <div class="bs-example" data-example-id="btn-variants">
                    <a type="button" class="btn btn-primary" href="https://bitbucket.org/Georges034302/dev-repo" target="_blank">BitBucket</a>
                    <a type="button" class="btn btn-success" href="https://app.codeship.com/georges-boughantous-purple-frog-63" target="_blank">Codeship</a>
                    <a type="button" class="btn btn-info" href="https://dashboard.heroku.com/apps/maven-app-heroku" target="_blank">Heroku</a>
                    <a type="button" class="btn btn-info" href="https://console.cloud.google.com/home/dashboard?project=propane-primacy-193602" target="_blank">GAE</a>
                    <a type="button" class="btn btn-info" href="https://ap-southeast-2.console.aws.amazon.com/console/home?region=ap-southeast-2" target="_blank">AWS</a>
                    <a type="button" class="btn btn-warning" href="https://app.slack.com/client/T45PZN094/C451YSREX" target="_blank">Slack</a>
                    <a type="button" class="btn btn-danger" href="https://papertrailapp.com/systems/maven-app-heroku/events" target="_blank">Papertrail</a>
                    <a type="button" class="btn btn-default" href="https://www.mlab.com/databases/heroku_59pxdn6j" target="_blank">Mongo DB</a>
                </div>                
            </div>
        </div>
         <script src="webjars/jquery/3.3.1/jquery.js"></script>
        <script src="webjars/bootstrap/3.3.5/js/bootstrap.js"></script>
    </body>
</html>