<%@page import="uts.wsd.*"%>
<%@page import="uts.wsd.iot.*"%>
<!-- Source: https://github.com/DamienFremont/blog/tree/master/20151007-javaee-webjars-->
<!-- Complete Dashboard source in bootstrap 4: https://github.com/modularcode/modular-admin-html-->
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="webjars/bootstrap/3.3.5/css/bootstrap.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script type="text/javascript" src="script.js"></script>  
        <link rel="stylesheet" type="text/css" href="style.css">        
    </head>

    <body>       
        <div class="bs-docs-header" id="content" tabindex="-1">
            <div class="container">
                <h1>DRA___ Framework for IOT</h1>
                <p>A DevOps Reference Architecture Pipeline for Multi-Cloud.</p>
            </div>
            <nav class="navbar navbar-inverse">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span> 
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="#Display-Div" data-toggle="tab" id="IOT-Network">IoT-Network</a>
                    </div>
                    <div class="collapse navbar-collapse">
                        <ul class="nav navbar-nav"> 
                            <li class="dropdown">
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">Author<span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a href="https://www.linkedin.com/in/georges-bou-ghantous/" target="_Blank" style="color:#cdbfe3;">Linked In</a></li>
                                    <li><a href="mailto:Georges.BouGhantous-1@uts.edu.au" style="color:#cdbfe3; ">Contact</a></li>
                                    <li><a href="https://www.uts.edu.au/" target="_Blank" style="color:#cdbfe3; ">Institution</a></li>
                                </ul>
                            </li>
                            <li class="dropdown">
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">Publications<span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a href="https://goo.gl/91XnCi" target="_Blank" style="color:#cdbfe3;">PACIS2017 - Conference</a></li>  
                                    <li><a href="https://goo.gl/Vy5aT9" target="_Blank" style="color:#cdbfe3;">IEEE-CBI2018 - Conference</a></li>
                                    <li><a href="https://drive.google.com/file/d/1QvCZVHLGCji2TRJFR_L-wT-MvsjKKyOt/view?usp=sharing" target="_Blank" style="color:#cdbfe3;">IJLTER2019-Journal</a></li>
                                </ul>
                            </li>                            
                            <li class="dropdown">
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">DRA 1.0<span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a href="https://goo.gl/nq8vJc" target="_Blank" style="color:#cdbfe3;">DRA v1.0 Project</a></li>                                    
                                    <li><a href="https://youtu.be/IJP9qYcatHs" target="_Blank" style="color:#cdbfe3; ">DRA v1.0: Demo Video</a></li>
                                    <li><a href="#Display-Div" data-toggle="tab" data-target="#Display-Div"style="color:#cdbfe3;" id="DRA1-View">DRAv1.0 View</a></li>                                     
                                </ul>
                            </li> 
                            <li class="dropdown">
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">DRA v2.0<span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a href="https://goo.gl/3PYfPh" target="_Blank" style="color:#cdbfe3;">DRA v2.0 Project</a></li>                                    
                                    <li><a href="https://youtu.be/DmrIAciPKAU" target="_Blank" style="color:#cdbfe3; ">DRA v2.0: Demo Video</a></li>
                                    <li><a href="#Display-Div" data-toggle="tab" data-target="#Display-Div"style="color:#cdbfe3;" id="DRA2-View">DRA2.0 View</a></li>                                     
                                </ul>
                            </li>
                            <li class="dropdown">
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">DRA+APEM<span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a href="https://goo.gl/fUE8hY" target="_Blank" style="color:#cdbfe3;">DRA+APEM Integration</a></li>                                 
                                    <li><a href="#Display-Div" data-toggle="tab" data-target="#Display-Div"style="color:#cdbfe3;" id="DRA3-View">DRA+APEM View</a></li>
                                </ul>
                            </li>
                            <li class="dropdown">
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">DRA Evaluation<span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a href="https://goo.gl/bM4w1t" target="_Blank" style="color:#cdbfe3;">Google Survey Form</a></li>  
                                    <li><a href="https://docs.google.com/document/d/1tx177JIdXKWm0CipS7ZtoYw65g88qtqpASAZC8vRStU/edit?usp=sharing" target="_Blank" style="color:#cdbfe3;">Case Study Template</a></li>  

                                </ul>
                            </li>
                        </ul>   
                        <%  String loginErr = (String) session.getAttribute("loginErr");%>                        
                        <ul class="nav navbar-nav navbar-right">                            
                            <li><a href="#login" id="login" >
                                    <span style="color:#00ffcc"><%if (loginErr != null) {%>${loginErr}<%} else {
                                            session.invalidate();
                                        }%>
                                    </span>&emsp;<span class="glyphicon glyphicon-log-in"></span> Login</a>
                            </li>
                        </ul>                         
                    </div>                        
                </div>
            </nav>

            <div class="row" id="login-div">
                <form class="navbar-form navbar-right form-horizontal" method="post" action="login_action.jsp" >
                    <div class="form-group input-group">
                        <label for="inputEmail" class="col-sm-2 control-label"  >Email</label>
                        <span class="input-group-btn ">                    
                            <input type="email" class="form-control" name="email" placeholder="Email">                           
                        </span> 
                        <label for="inputPassword" class="col-sm-2 control-label"  >Password</label>
                        <span class="input-group-btn">
                            <input type="password" class="form-control" name="password" placeholder="Password">
                        </span>
                        &emsp;
                        <span class="input-group-btn">
                            <input type="submit" class="btn btn-success" value="Sign in" id="signin">
                        </span>
                        &emsp;&emsp;&emsp;
                    </div>
                </form>                
            </div>           
        </div>

        <div class="container" style="width:100%; height: 70%; " id="IoT-div">
            <div class="row">                                  
                <div class="tab-content" id="Display-Div">
                    <iframe id="IOT-frame" src="" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe> 
                </div>                
            </div>
        </div>

        <!-- IoT Control Panel -->
        <div class="footer">            
            <h2 id="buttons-options">IoT Network Control Panel</h2>
            <div class="bs-glyphicons" style="padding-bottom:10px;">                            
                <a class="btn btn-default" href="IOTServlet?iotplay" id="play">                        
                    <span class="glyphicon glyphicon-play" aria-hidden="true"></span>                                             
                </a>                 
                <a class="btn btn-default" href="IOTServlet?iotdata" onclick="reload()" id="refresh">
                    <span class="glyphicon glyphicon-refresh" aria-hidden="true"></span>
                </a>                
                <a class="btn btn-default" href="IOTServlet?ioton">
                    <span class="glyphicon glyphicon-plus-sign " aria-hidden="true" id="start"></span>
                </a>               
                <a class="btn btn-default" href="IOTServlet?iotoff">
                    <span class="glyphicon glyphicon-minus-sign" aria-hidden="true" id="stop"></span>
                </a>               
                <a class="btn btn-default" href="#" download>
                    <span class="glyphicon glyphicon-save" aria-hidden="true" id="download"></span>
                </a>               
                <a class="btn btn-default" href="https://www.google.com/drive/" target="_blank" id="gcloud">
                    <span class="glyphicon glyphicon-cloud" aria-hidden="true"></span>
                </a>            
                <a class="btn btn-default" href="http://collabedit.com/" target="_blank" id="pencil">
                    <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                </a>           
                <a class="btn btn-default" href="mailto: ">
                    <span class="glyphicon glyphicon-send" aria-hidden="true" id="mail"></span>
                </a>
            </div>
        </div>  
        <script src="webjars/jquery/3.3.1/jquery.js"></script>
        <script src="webjars/bootstrap/3.3.5/js/bootstrap.js"></script>
    </body>
</html>