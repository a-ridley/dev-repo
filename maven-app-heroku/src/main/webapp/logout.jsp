<%-- 
    Document   : logout
    Created on : Aug 10, 2016, 2:40:01 PM
    Author     : George
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Logout</title>
    </head>
    <body bgcolor="lightblue">
        <%
            session.invalidate();
            response.sendRedirect("index.jsp");
            %>
        <p>You have been logged out. Click <a href="index.jsp">here</a> to return to the main page.</p>
    </body>
</html>
