//Active Login 
$(document).ready(function () {
    $("#login-div").hide();
    $("#login").click(function () {
        $("#login-div").toggle();
    });

});

//reload iframe
function reload() {
    document.getElementById('IOT-frame').src += '';
}

//Select view
$(function () {
    $("#IOT-Network").click(function () {
        $("#IOT-frame").attr("src", "iot_net.jsp");
    });
    $("#DRA1-View").click(function () {
        $("#IOT-frame").attr("src", "dra1.jsp");
    });
    $("#DRA2-View").click(function () {
        $("#IOT-frame").attr("src", "dra2.jsp");
    });
    $("#DRA3-View").click(function () {
        $("#IOT-frame").attr("src", "dra3.jsp");
    });
});


//Bind iframe to sources
$('#myNavbar').bind('show', function (e) {
    paneID = $(e.target).attr('href');
    src = $(paneID).attr('data-src');
    // if the iframe hasn't already been loaded once
    if ($(paneID + " iframe").attr("src") === "")
    {
        $(paneID + " iframe").attr("src", src);
    }
});

//Submit form on click
function submitForm(form) {
    var myform = document.getElementById(form);
    myform.submit();
}

