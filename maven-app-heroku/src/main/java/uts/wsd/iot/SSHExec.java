/*
 * Author: Georges Bou Ghantous
 *
 * This class provides the methods to establish SSH connection to Raspberry Pi Model B (RPIB)
 * by opening an SSH tunnel to port 22 using the IP of the RAPIB on the network where the device
 * is located. Then create an SSH session to process shell commands on RPID Linux OS.
 */
package uts.wsd.iot;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.schmizz.sshj.SSHClient;
import net.schmizz.sshj.common.IOUtils;
import net.schmizz.sshj.connection.channel.direct.Session;
import net.schmizz.sshj.connection.channel.direct.Session.Command;
import uts.wsd.controller.MongoDBConnector;

public class SSHExec {

    public String result;
    private int numThreads = 3;
    private ExecutorService executor = Executors.newFixedThreadPool(numThreads);
        
    public SSHExec() {
    }

    public String cmdResults() {
        return result;
    }

    //Get Pin status after executing an SSH command
    public String getPinStatus(){
        return (cmdResults().trim().equals("1")) ? "ON" : "OFF";
    }
    
    //Establish connection to MongoDBLab cloud Database cloud
    //Save IoT pin status (Sensor status) as JSON format
    public void saveIoTData(String sensor, String status) throws UnknownHostException{
        MongoDBConnector db = new MongoDBConnector();
        db.saveIoTData(sensor, getPinStatus());
        db.close();
    }
    
    //fire an SSH command to execute startLED.py to stop the Sensor_A
    //Sensor_A status is saved to MongoDBLab cloud data base as JSON format
    //dynamically using saveIoTData method.
    public void startLED() throws Exception {
        fireCommand("sudo python startLED.py");       
        System.out.println("=========================");
        System.out.println("Start LED Output:");
        System.out.println("-------------------------");
        fireCommand("gpio -g read 13");
        System.out.println("LED Lights: " + getPinStatus()); 
        saveIoTData("LED",getPinStatus());
    }

    //fire an SSH command to execute stopLED.py to stop the Sensor_A
    //Sensor_A status is saved to MongoDBLab cloud data base as JSON format
    //dynamically using saveIoTData method.
    public void stopLED() throws Exception {
        fireCommand("sudo python stopLED.py");        
        System.out.println("=========================");
        System.out.println("Start LED Output:");
        System.out.println("-------------------------");
        fireCommand("gpio -g read 13");
        System.out.println("LED Lights: " + getPinStatus()); 
        saveIoTData("LED",getPinStatus());
    }

    //Create a background runnable thread to read GPIO pin 12 status 
    //connected to a motion sensor by firing .
    //The executor runs the thread in the background.
    //Sensor_B status is saved to MongoDBLab cloud data base as JSON format
    //dynamically using saveIoTData method.
    public void readMotionSensor() throws UnknownHostException {
        System.out.println("=========================");
        System.out.println("Motion Sensor Output:");
        System.out.println("-------------------------");

        Runnable motionsensorTask = () -> {
            try {
                fireCommand("gpio -g read 12");
                
                System.out.println("Motion Sensor Light: " +getPinStatus());
                saveIoTData("Motion Sensor",getPinStatus());
            } catch (Exception ex) {
                Logger.getLogger(SSHExec.class.getName()).log(Level.SEVERE, null, ex);
            }            
        };
        int i = 0;
        while(i++< 5) executor.execute(motionsensorTask);
            
        System.out.println("=========================");        
    }

    public void stopSession(){
        executor.shutdown();
    }
    
    //Creates an SSH client to tunnel to RPIB using ssh on port 22. 
    //Then establishes connection to the RPIB using its IP address on a given network.
    //Then use the SSH client to start an SSH session and execute shell commands on RPID 
    //Linux OS. The results are collected by converting the command output into string.
    public void fireCommand(String command) throws Exception {
        final SSHClient ssh = new SSHClient();
        Command cmd = null;

        ssh.addHostKeyVerifier(new NullHostKeyVerifier());

        int pn = 22;        
        String ipaddress = "1.41.76.29";
        String username = "pi";
        String password = "georges034302";
        
        try {
            ssh.connect(ipaddress, pn);
        } catch (IOException e) { }

        ssh.authPassword(username, password);
        final Session session = ssh.startSession();
        cmd = session.exec(command);
        result = IOUtils.readFully(cmd.getInputStream()).toString();
        ssh.close();
    }
}
