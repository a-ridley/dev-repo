package uts.wsd.iot;

/**
 * Run Shell commands using SSH to execute python script on Raspberry Pi (RPIB)
 * Activate Sensor_A and Sensor_B using firecommand from SSHExec
 * * @author George
 */
public class RunIOT {    
    
    public static void main(String[] args) throws Exception {
        SSHExec ssh = new SSHExec();       
        ssh.startLED();
        ssh.stopLED();
        ssh.readMotionSensor();
        ssh.stopSession();
    }    
}
