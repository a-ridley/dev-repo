package uts.wsd;

import java.io.Serializable;
import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD) // Use the defaults for fields
@XmlRootElement(name = "user")
public class User implements Serializable {
    // The defaults are OK
    // Each of these fields will be automatically mapped to an XML Element of the same name.

    private String email; // eg. this field will be mapped to an XML element called "email"
    private String name;
    private String password;   

    public User() {
        super();
    }

    public User(String email, String name, String password) {
        super();
        this.email = email;
        this.name = name;
        this.password = password;       
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;        
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
   
}
