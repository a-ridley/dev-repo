/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uts.wsd.test;

/**
 *
 * @author George
 */
import cucumber.api.PendingException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.*;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import io.github.bonigarcia.wdm.WebDriverManager;

public class CucumberTest {

    WebDriver driver;

    @Given("^Open Chrome browser$")
    public void open_Chrome_browser() throws Throwable {
        Class<? extends WebDriver> driverClass = FirefoxDriver.class;
        //Class<? extends WebDriver> driverClass = ChromeDriver.class;
        WebDriverManager.getInstance(driverClass).setup();
        driver = driverClass.newInstance();
        driver.get("https://maven-app-heroku.herokuapp.com/");

        // throw new PendingException();
    }

    @When("^maven-app-heroku is loaded$")
    public void maven_app_heroku_is_loaded() throws Throwable {
        driver.navigate().to(driver.getCurrentUrl());
    }

    @Then("^login button enabled$")
    public void login_button_enabled() throws Throwable {
        if (driver.findElement(By.id("login")).isEnabled()) {
            System.out.println("Login button enabled.");
        } else {
            System.out.println("Login button disabled");
        }
        driver.close();
    }

    @Then("^play button enabled$")
    public void play_button_enabled() throws Throwable {
        if (driver.findElement(By.id("play")).isEnabled()) {
            System.out.println("Play button enabled.");
        } else {
            System.out.println("Play button disabled");
        }
        driver.close();
        //throw new PendingException();
    }

    @Then("^refresh button enabled$")
    public void refresh_button_enabled() throws Throwable {
        if (driver.findElement(By.id("refresh")).isEnabled()) {
            System.out.println("Refresh button enabled.");
        } else {
            System.out.println("Refresh button disabled");
        }
        driver.close();
        //throw new PendingException();
    }

    @Then("^start button enabled$")
    public void start_button_enabled() throws Throwable {
        if (driver.findElement(By.id("start")).isEnabled()) {
            System.out.println("Start button enabled.");
        } else {
            System.out.println("Start button disabled");
        }
        driver.close();
        //throw new PendingException();
    }

    @Then("^stop button enabled$")
    public void stop_button_enabled() throws Throwable {
        if (driver.findElement(By.id("stop")).isEnabled()) {
            System.out.println("Stop button enabled.");
        } else {
            System.out.println("Stop button disabled");
        }
        driver.close();
        //throw new PendingException();
    }
}
