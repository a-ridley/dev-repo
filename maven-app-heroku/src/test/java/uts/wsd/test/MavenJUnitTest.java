/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uts.wsd.test;

import java.net.*;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;
import uts.wsd.controller.MongoDBConnector;
import uts.wsd.iot.SSHExec;

/**
 *
 * @author George
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class MavenJUnitTest {
   
    private static MongoDBConnector mdb;
    private static SSHExec sshExec;
   
    public MavenJUnitTest() {  }

    @Test
    public void testMongoDBConnect() throws UnknownHostException {
        Assert.assertNotNull("Cannot establish connection to MDB",mdb);        
        System.out.println("\n- Connecting to MongoDB ");
    }
    @Test
    public void testSSHExec() {
        Assert.assertNotNull("Cannot open SSH Session",sshExec);       
        System.out.println("\n- Creating SSH Session" );
    }    
    @BeforeClass
    public static void setUpClass() throws UnknownHostException {
        System.out.println("\n<-- Starting test -->");  
        mdb = new MongoDBConnector();
        sshExec = new SSHExec();
    }
    @AfterClass
    public static void tearDownClass() {
        System.out.print("\n<-- Test Finished : ");        
    }     
}
