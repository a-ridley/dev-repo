Feature: Test open mave-app-heroku URL

  Scenario: Test login button enabled
    Given Open Chrome browser
    When maven-app-heroku is loaded
    Then login button enabled

  Scenario: Test play button enabled
    Given Open Chrome browser
    When maven-app-heroku is loaded
    Then play button enabled

  Scenario: Test refresh button enabled
    Given Open Chrome browser
    When maven-app-heroku is loaded
    Then refresh button enabled

  Scenario: Test start button enabled
    Given Open Chrome browser
    When maven-app-heroku is loaded
    Then start button enabled

  Scenario: Test stop button enabled
    Given Open Chrome browser
    When maven-app-heroku is loaded
    Then stop button enabled
