#DRA V2.0 Pipeline for Multi-Cloud IoT-Apps:
============================================


-Bitbucket
-Codeship
-Heroku
-mLab MongoDB
-Papertrail
-HipChat
-Google Engine App 
{
	application-type: Maven-java
	add: appengine-web.xml to WEB-INF (make sure it contains <runtime> Java 8</runtime>)
	point Codeship deployment PATH for GAE to Maven-app target/SNAPSHOT where WEB-INF is           located)
}

-AWS CodeDeploy

Refer To BitBucket/dev-repo for more details about maven-app-heroku (IoT application)

Please Note:
maven-app-heroku interacts with RPIB (IP address) using SSH to send commmands and run the python applications below.

Refer to the "Python Apps" for python applications installed on Raspberry Pi (RPIB):
blink.py
startLED.py
stopLED.py

To view DRA testing and demo visit our your tube channel: https://youtu.be/phy47ZHjZck
