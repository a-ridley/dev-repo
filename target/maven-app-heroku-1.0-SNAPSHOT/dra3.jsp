<%-- 
    Document   : iot_net
    Created on : Sep 20, 2017, 9:03:16 PM
    Author     : George
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="webjars/bootstrap/3.3.5/css/bootstrap.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <title>DRA View</title>
    </head>
    <body>
        <div class="container" style="width:100%; height: 70%; ">           
            <img class="img-fluid" src="DRAGill.png" style="width:50%; height: 20%;">
        </div>
    </div>
    <script src="webjars/jquery/3.3.1/jquery.js"></script>
    <script src="webjars/bootstrap/3.3.5/js/bootstrap.js"></script>
</body>
</html>
