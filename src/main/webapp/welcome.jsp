<%@page import="uts.wsd.controller.MongoDBConnector"%>
<%@page import ="uts.wsd.*" contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Welcome</title>
    </head>
    
    <%
        String email = request.getParameter("email");
        String name = request.getParameter("name");
        String password = request.getParameter("password");
    %>
    <body bgcolor="lightblue">
        <h1>Welcome!</h1>
        <p>Updating Mlab DB</p>
        
        <p>Your Email is: <%=email%></p>
        <p>Your Name is: <%=name%></p>
        <p>Your Password  is: <%=password%></p>
        <%
            User user = new User(email,name,password);
            session.setAttribute("user", user);
            MongoDBConnector db = new MongoDBConnector();
            db.add(user);
            db.close();
            response.sendRedirect("main.jsp");
        %>
        <p>Click <a href="main.jsp"> here </a> to proceed to main page.</p>
    </body>
</html>
