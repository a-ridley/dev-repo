<%-- 
    Document   : iot_net
    Created on : Sep 20, 2017, 9:03:16 PM
    Author     : George
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="uts.wsd.*"%>
<%@page import="uts.wsd.iot.*"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="webjars/bootstrap/3.3.5/css/bootstrap.css">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>        
        <title>IOT Network</title>
        <style>
            th {
                padding-top: 12px;
                padding-bottom: 12px;
                text-align: center;                
                color: white;
                font-size:18px;
                font-weight: bold;
            }
            td{
                text-align: center;
            }
        </style>
    </head>
    <body>    
        <br>
        <div class="container-fluid" style="width:100%">            
            <div class="row">
                <div class="col-xs-7 col-sm-6 col-lg-8">                   
                    <table class="table">
                        <thead>
                            <tr style="background-color: #f4a041;"><th colspan="5" style="font-size:24px;">IOT Device Network</th></tr>
                            <tr style="background-color: #4CAF50;">
                                <th>#</th>
                                <th>Device Type</th>
                                <th>Name</th>
                                <th>Control Button</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td scope="row" style="font-size:18px;font-weight: bold;">1</td>
                                <td>Light Sensor</td> 
                                <td>Sensor A</td>
                                <td><i class="material-icons" style="font-size:36px">play_arrow</i><i class="material-icons" style="font-size:36px">add_circle</i><i class="material-icons" style="font-size:36px">remove_circle</i></td>
                                <td>
                                    4 LED lights (multi-colored) connected to 4 GPIO pins [13, 17, 19, 22] 
                                    on a Raspberry Pi Model 3 B (named RPIB).
                                </td>
                            </tr>
                            <tr>
                                <td scope="row" style="font-size:18px;font-weight: bold;">2</td>
                                <td>Motion Sensor</td>
                                <td>Sensor B</td>
                                <td><i class="material-icons" style="font-size:36px">refresh</i></td>
                                <td> 
                                    Motion Sensor + 1 LED connection to a single GPIO pin [12] 
                                    on a Raspberry Pi Model 3 B (named RPIB). 
                                </td>
                            </tr>                            
                        </tbody>
                    </table>
                </div>
            </div>        
        </div>
        <br><br><br>
        <div class="container-fluid" style="width:100%">            
            <div class="row">
                <div class="col-xs-7 col-sm-6 col-lg-8">                    
                    <table class="table">
                        
                        <thead>       
                            <tr style="background-color: #f4a041;"><th colspan="5" style="font-size:24px;">IOT Control Script</th></tr>
                            <tr style="background-color: #42c5f4;">
                                <th>#</th>
                                <th>Script</th>
                                <th>Language</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td scope="row" style="font-size:18px;font-weight: bold;">1</td>
                                <td>blink.py</td> 
                                <td>Python</td>
                                <td>RPIB (embedded)</td>
                                <td>
                                    Activate Sensor_A to blink (ON/OFF) 10 times each in sequential pattern
                                </td>
                            </tr>
                            <tr>
                                <td scope="row" style="font-size:18px;font-weight: bold;">2</td>
                                <td>start.py</td>
                                <td>Python</td>
                                <td>RPIB (embedded)</td>
                                <td> 
                                    Turn on Sensor_A. IoT-data is collected (data=ON)  
                                </td>
                            </tr> 
                            <tr>
                                <td scope="row" style="font-size:18px;font-weight: bold;">3</td>
                                <td>stop.py</td>
                                <td>Python</td>
                                <td>RPIB (embedded)</td>
                                <td> 
                                    Turn off Sensor_A. IoT-data is collected (data=OFF)
                                </td>
                            </tr>
                            <tr>
                                <td scope="row" style="font-size:18px;font-weight: bold;">4</td>
                                <td>readMotionSensor()</td>
                                <td>Java/Unix Shell </td>
                                <td>uts.wsd.iot.SSHExec.java</td>
                                <td> 
                                    Collect IoT-data from Sensor_B. Every time it detects a motion data=ON else data=OFF 
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>        
        </div>
        <script src="webjars/jquery/3.3.1/jquery.js"></script>
        <script src="webjars/bootstrap/3.3.5/js/bootstrap.js"></script>
    </body>
</html>
