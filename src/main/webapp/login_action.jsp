<%-- 
    Document   : login_action
    Created on : Sep 19, 2017, 12:37:30 AM
    Author     : George
--%>

<%@page import="uts.wsd.*"%>
<%@page import="uts.wsd.controller.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>   
    <body>
        <% String filePath = application.getRealPath("WEB-INF/users.xml");%>
        <jsp:useBean id="userApp" class="uts.wsd.controller.UserApplication" scope="application">
            <jsp:setProperty name="userApp" property="filePath" value="<%=filePath%>"/>
        </jsp:useBean>
        <%
            String email = request.getParameter("email");
            String password = request.getParameter("password");
            Users users = userApp.getUsers();
            User user = users.login(email,password);
            if(user != null){
                session.setAttribute("user", user);
                response.sendRedirect("main.jsp");
            }else{
                String loginErr = "User does not exist!";
                session.setAttribute("loginErr", loginErr);
                response.sendRedirect("index.jsp");
            }
        %>
    </body>
</html>
