/*
 * Author: Georges Bou Ghantous
 *
 * This class provides the methods to establish connection between maven-app-heroku
 * and MongoDBLab cloud Database and save IoT generated data at button click on injex.jsp.
 * The python scripts excuted remotely using SSHExec.java methods on RPIB collect data from
 * sensors (Sensor_A and Sensor_B). The data is saved dynamically on mLab cloud database as
 * as JSON format.
 */
package uts.wsd.controller;

import java.net.UnknownHostException;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.MongoCollection;
import org.bson.Document;
import java.util.*;
import uts.wsd.*;

public class MongoDBConnector {
    
    private List<Document> users = new ArrayList();
    private MongoClientURI uri ;
    private MongoClient client ;
    private MongoDatabase db;
    private List<Document> data = new ArrayList();
    
    public MongoDBConnector() throws UnknownHostException { connect(); }
    
    private void connect() throws UnknownHostException{
        uri  = new MongoClientURI("mongodb://demo:Password1@ds121189.mlab.com:21189/heroku_t85fwctj"); 
        client = new MongoClient(uri);
        db = client.getDatabase(uri.getDatabase());
    }
    public void add(User user){
        users.add(new Document("Username",user.getEmail()).append("Password", user.getPassword()));
        MongoCollection<Document> userlist = db.getCollection("users");
        userlist.insertMany(users);
    }
    
    public void saveIoTData(String pin, String data){
        this.data.add(new Document(pin,data));
        MongoCollection<Document> iotData = db.getCollection("iotdata");
        iotData.insertMany(this.data);
    }
    
    public void close(){ client.close(); }    
    public MongoClient getClient(){ return client; }
    public MongoDatabase createMDB(){ return db; }
}
