package uts.wsd.controller;


import java.io.*;
import javax.xml.bind.*;
import uts.wsd.*;

public class UserApplication implements Serializable {

    private String filePath;
    private Users users;

    public String getFilePath() {
        return filePath;
    }

    //Unmarshall XML into Javabean
    public void setFilePath(String filePath) throws JAXBException, IOException {
        this.filePath = filePath;        
        JAXBContext jc = JAXBContext.newInstance(Users.class);
        Unmarshaller u = jc.createUnmarshaller();
        FileInputStream fin = new FileInputStream(filePath); 
        users = (Users) u.unmarshal(fin); 
        fin.close();
    }

    public Users getUsers() {
        return users;
    }

    public void setUsers(Users users) {
        this.users = users;
    }
    
    public void saveUsers() throws JAXBException, IOException {
        JAXBContext jc = JAXBContext.newInstance(Users.class);
        Marshaller m = jc.createMarshaller();
        m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        FileOutputStream fout = new FileOutputStream(filePath);
        m.marshal(users, fout);
        fout.close();
    }
}
