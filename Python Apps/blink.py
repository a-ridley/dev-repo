import RPi.GPIO as GPIO
import time

GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)
xmas_led = (13,17,19,22)
GPIO.setup(xmas_led,GPIO.OUT)
j=0
while j<10:
    for i in range(len(xmas_led)):
        GPIO.output(xmas_led[i],True)
        time.sleep(0.05)
        GPIO.output(xmas_led[i],False)
        time.sleep(0.05)
    j+=1
GPIO.cleanup()

