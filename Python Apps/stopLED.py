import RPi.GPIO as GPIO
import time

GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)
xmas_led = (13,17,19,22)
GPIO.setup(xmas_led,GPIO.OUT)

for i in range(len(xmas_led)):
    GPIO.output(xmas_led[i],False)

